﻿using AutoMapper;

public class TaskProfile : Profile
{
    public TaskProfile()
    {
        CreateMap<Task, TaskDto>();
        CreateMap<TaskDto, Task>();
        CreateMap<NestedTask, TaskDto>();
    }
}
