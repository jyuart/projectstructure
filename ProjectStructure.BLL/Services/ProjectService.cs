﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

public class ProjectService
{
    private readonly IRepository<Project> _repository;
    private readonly IMapper _mapper;
    public ProjectService(IRepository<Project> repository, IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }

    public async Task<List<ProjectDto>> GetAllProjects()
    {
        var result = await _repository.GetAll();
        return _mapper.Map<List<ProjectDto>>(result);
    }

    public async Task<ProjectDto> GetProject(int id)
    {
        var result = await _repository.Get(id);
        return _mapper.Map<ProjectDto>(result);
    }

    public async System.Threading.Tasks.Task PostProject(ProjectDto projectDto)
    {
        var project = _mapper.Map<Project>(projectDto);
        foreach (var prop in project.GetType().GetProperties())
        {
            if (prop.GetValue(project) == null)
            {
                if (prop.Name == "Id")
                    continue;
                throw new ArgumentException("Object properties can't be null");
            }
        }
        await _repository.Add(project);
        await _repository.Save();
    }

    public async System.Threading.Tasks.Task UpdateProject(ProjectDto projectDto)
    {
        var project = _mapper.Map<Project>(projectDto);
        await _repository.Update(project);
        await _repository.Save();
    }

    public async System.Threading.Tasks.Task DeleteProject(int id)
    {
        await _repository.Delete(id);
        await _repository.Save();
    }
}
