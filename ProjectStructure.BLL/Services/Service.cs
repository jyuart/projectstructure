using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

public class Service
{
    private readonly IRepository<Project> _projectsRepository;
    private readonly IRepository<Task> _tasksRepository;
    private readonly IRepository<User> _usersRepository;
    private readonly IRepository<Team> _teamsRepository;

    private readonly IMapper _mapper;

    public Service(IRepository<Project> projectsRepository, IRepository<Task> tasksRepository, IRepository<User> usersRepository, IRepository<Team> teamsRepository, IMapper mapper)
    {
        _projectsRepository = projectsRepository;
        _tasksRepository = tasksRepository;
        _usersRepository = usersRepository;
        _teamsRepository = teamsRepository;

        _mapper = mapper;
    }

    // Получить количество тасков у проекта конкретного пользователя (по id пользователя)
    public async Task<List<TasksPerUser>> GetTasksInUserProjects(int id)
    {
        var _nestedStructure = await GetNestedStructure();

        var result = (from project in _nestedStructure
                      where project.AuthorId == id && project.Tasks.Any()
                      select new TasksPerUser() { Project = _mapper.Map<ProjectDto>(project), TasksAmount = project.Tasks.Count }).ToList();
        return result;
    }

    // Получить количество тасков, назначенных на конкретного пользователя с длиной name < 45 (по id)
    public async Task<List<TaskDto>> GetAllUserTasks(int id)
    {
        var _nestedStructure = await GetNestedStructure();

        var result = from project in _nestedStructure
                     from task in project.Tasks
                     where task.PerformerId == id && task.Name.Length < 45
                     select task;
        var resultDto = _mapper.Map<IEnumerable<TaskDto>>(result);
        return resultDto.ToList();
    }

    // Получить список (id, name) из коллекции выполненных в 2020 году тасков для конкретного пользователя (по id)
    public async Task<List<FinishedThisYear>> GetAllUserTasksFinishedThisYear(int id)
    {
        var _nestedStructure = await GetNestedStructure();

        var result = (from project in _nestedStructure
                      from task in project.Tasks
                      where (TaskState)task.State == TaskState.Finished && task.FinishedAt.Year == 2020 && task.PerformerId == id
                      select new FinishedThisYear() { Id = task.Id, Name = task.Name }).ToList();
        return result;
    }

    // Получить список из коллекции команд, участники которых старше 10 лет, отсортированных по дате регистрации пользователя по убыванию, а также сгруппированных по командам.
    public async Task<List<TeamWithUsersOlderTen>> GetTeamsWithUsersOlderTen() 
    {
        var _allUsers = await _usersRepository.GetAll();
        var _allTeams = await _teamsRepository.GetAll();
        
        var result = (from team in _allTeams
                     join user in _allUsers on team.Id equals user.TeamId into usersInTeam
                     where usersInTeam.Any() && usersInTeam.All(x=>x.Birthday.Year < 2010)
                     select new TeamWithUsersOlderTen() { Id = team.Id, Name = team.Name, Users = _mapper.Map<List<UserDto>>(usersInTeam.OrderByDescending(x => x.RegisteredAt).ToList())}).ToList();
        return result;
    }

    // Получить список пользователей по алфавиту first_name (по возрастанию) с отсортированными tasks по длине name (по убыванию).
    public async Task<List<UserTasks>> GetAllUsersWithTasks()
    {
        var _allUsers = await _usersRepository.GetAll();
        var _allTasks = await _tasksRepository.GetAll();

        var result = (from user in _allUsers
                      join task in _allTasks on user.Id equals task.PerformerId into usersTasks
                      orderby user.FirstName
                      where usersTasks.Any()
                      select new UserTasks() { User = _mapper.Map<UserDto>(user), Tasks = _mapper.Map<List<TaskDto>>(usersTasks.OrderByDescending(x => x.Name.Length).ToList())}).ToList();
        return result;
    }

    // Получить структуру с подробностями о пользователе
    public async Task<UserDetails> GetUserDetails(int id)
    {
        var _allProjects = await _projectsRepository.GetAll();
        var _allTasks = await _tasksRepository.GetAll();

        var user = await _usersRepository.Get(id);
        var result = (from project in _allProjects.Where(x => x.AuthorId == user.Id)
                      join task in _allTasks on project.Id equals task.ProjectId
                      select new
                      {
                          User = user,
                          LastProject = _allProjects.Where(x => x.AuthorId == user.Id).OrderByDescending(x => x.CreatedAt).FirstOrDefault(),
                          ProjectTasks = _allTasks.Count(x => x.ProjectId == (_allProjects.Where(x => x.AuthorId == user.Id).OrderByDescending(x => x.CreatedAt)).FirstOrDefault()?.Id),
                          NotFinishedTasks = _allTasks.Count(x => x.PerformerId == user.Id && (TaskState)x.State != TaskState.Finished),
                          LongestTask = _allTasks.Where(x => x.PerformerId == user.Id).Aggregate((x, y) => (x.FinishedAt - x.CreatedAt) > (y.FinishedAt - y.CreatedAt) ? x : y)
                      }).FirstOrDefault();

        var resultDto = new UserDetails()
        {
            User = _mapper.Map<UserDto>(result.User),
            LastProject = _mapper.Map<ProjectDto>(result.LastProject),
            ProjectTasks = result.ProjectTasks,
            NotFinishedTasks = result.NotFinishedTasks,
            LongestTask = _mapper.Map<TaskDto>(result.LongestTask)
    };

        if (resultDto != null)
        {
            return resultDto;
        }
        else
        {
            var empty = new UserDetails();
            return empty;
        }
    }

    // Получить структуру с подробностями о проекте
    public async Task<List<ProjectDetails>> GetProjectsDetails()
    {
        var _allProjects = await _projectsRepository.GetAll();
        var _allTasks = await _tasksRepository.GetAll();
        var _allUsers = await _usersRepository.GetAll();
        var _allTeams = await _teamsRepository.GetAll();
        var result = (from project in _allProjects
                    join task in _allTasks on project.Id equals task.ProjectId into tasksInProject
                    join team in _allTeams on project.TeamId equals team.Id
                    join user in _allUsers on team.Id equals user.TeamId into someUsers
                    where tasksInProject.Any()
                    select new ProjectDetails() 
                    { 
                         Project = _mapper.Map<ProjectDto>(project),
                         LongestTask = _mapper.Map<TaskDto>(tasksInProject.OrderByDescending(x => x.Description.Length).FirstOrDefault()),
                         ShortestTask = _mapper.Map<TaskDto>(tasksInProject.OrderByDescending(x => x.Name.Length).FirstOrDefault()),
                         Members = someUsers.Count(x => (x.TeamId == project.TeamId && project.Description.Length > 20) || tasksInProject.Count() < 3)
                    }).ToList();
        return result;
    }

    // Структура из вложенных объектов
    public async Task<List<NestedProject>> GetNestedStructure() {
        var _allProjects = await _projectsRepository.GetAll();
        var _allTasks = await _tasksRepository.GetAll();
        var _allUsers = await _usersRepository.GetAll();
        var _allTeams = await _teamsRepository.GetAll();

        var result = from project in _allProjects
                    join task in 
                    (from task in _allTasks
                    join user in _allUsers on task.PerformerId equals user.Id 
                    select new NestedTask {
                        Id = task.Id,
                        Name = task.Name,
                        Description = task.Description,
                        CreatedAt = task.CreatedAt,
                        FinishedAt = task.FinishedAt,
                        State = task.State,
                        ProjectId = task.ProjectId,
                        PerformerId = task.PerformerId,
                        Performer = user
                    })
                    on project.Id equals task.ProjectId into tasksInProject
                    join user in _allUsers on project.AuthorId equals user.Id
                    join team in _allTeams on project.TeamId equals team.Id
                    select new NestedProject 
                    {
                        Id = project.Id,
                        Name = project.Name,
                        Description = project.Description,
                        CreatedAt = project.CreatedAt,
                        Deadline = project.Deadline,
                        AuthorId = project.AuthorId,
                        Author = user,
                        TeamId = project.TeamId,
                        Tasks = tasksInProject.ToList(),
                        Team = team                     
                    };
        return result.ToList();
    }

    // Все невыполненные задания пользователя из всех проектов
    public async Task<List<TaskDto>> GetNotFinishedTasks(int id)
    {
        var _nestedStructure = await GetNestedStructure();
        var result = from project in _nestedStructure
                     from task in project.Tasks.Distinct()
                     where task.PerformerId == id && task.State != (int)TaskState.Finished
                     select task;
        var resultDto = _mapper.Map<List<TaskDto>>(result);
        return resultDto;
    }
}