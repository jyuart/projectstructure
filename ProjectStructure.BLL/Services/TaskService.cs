﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

public class TaskService
{
    private readonly IRepository<Task> _repository;
    private readonly IMapper _mapper;
    public TaskService(IRepository<Task> repository, IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }

    public async Task<List<TaskDto>> GetAllTasks()
    {
        var result = await _repository.GetAll();
        return _mapper.Map<List<TaskDto>>(result);
    }

    public async Task<TaskDto> GetTask(int id)
    {
        var result = await _repository.Get(id);
        return _mapper.Map<TaskDto>(result);
    }

    public async System.Threading.Tasks.Task PostTask(TaskDto taskDto)
    {
        var task = _mapper.Map<Task>(taskDto);
        foreach (var prop in task.GetType().GetProperties())
        {
            if (prop.GetValue(task) == null)
            {
                if (prop.Name == "Id")
                    continue;
                throw new ArgumentException("Object properties can't be null");
            }
        }
        await _repository.Add(task);
        await _repository.Save();
    }

    public async System.Threading.Tasks.Task UpdateTask(TaskDto taskDto)
    {
        var task = _mapper.Map<Task>(taskDto);
        await _repository.Update(task);
        await _repository.Save();
    }

    public async System.Threading.Tasks.Task DeleteTask(int id)
    {
        await _repository.Delete(id);
        await _repository.Save();
    }
}
