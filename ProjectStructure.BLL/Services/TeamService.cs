﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

public class TeamService
{
    private readonly IRepository<Team> _repository;
    private readonly IMapper _mapper;
    public TeamService(IRepository<Team> repository, IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }

    public async Task<List<TeamDto>> GetAllTeams()
    {
        var result = await _repository.GetAll();
        return _mapper.Map<List<TeamDto>>(result);
    }

    public async Task<TeamDto> GetTeam(int id)
    {
        var result = await _repository.Get(id);
        return _mapper.Map<TeamDto>(result);
    }

    public async System.Threading.Tasks.Task PostTeam(TeamDto teamDto)
    {
        var team = _mapper.Map<Team>(teamDto);
        foreach (var prop in team.GetType().GetProperties())
        {
            if (prop.GetValue(team) == null)
            {
                if (prop.Name == "Id")
                    continue;
                throw new ArgumentException("Object properties can't be null");
            }
        }
        await _repository.Add(team);
        await _repository.Save();
    }

    public async System.Threading.Tasks.Task UpdateTeam(TeamDto teamDto)
    {
        var team = _mapper.Map<Team>(teamDto);
        await _repository.Update(team);
        await _repository.Save();
    }

    public async System.Threading.Tasks.Task DeleteTeam(int id)
    {
        await _repository.Delete(id);
        await _repository.Save();
    }
}
