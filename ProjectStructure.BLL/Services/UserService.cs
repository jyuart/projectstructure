﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

public class UserService
{
    private readonly IRepository<User> _repository;
    private readonly IMapper _mapper;
    public UserService(IRepository<User> repository, IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }

    public async Task<List<UserDto>> GetAllUsers()
    {
        var result = await _repository.GetAll();
        return _mapper.Map<List<UserDto>>(result);
    }

    public async Task<UserDto> GetUser(int id)
    {
        var result = await _repository.Get(id);
        return _mapper.Map<UserDto>(result);
    }

    public async System.Threading.Tasks.Task PostUser(UserDto userDto)
    {
        var user = _mapper.Map<User>(userDto);
        foreach(var prop in user.GetType().GetProperties())
        {
            if (prop.GetValue(user) == null)
            {
                if (prop.Name == "TeamId" || prop.Name == "Id")
                    continue;
                throw new ArgumentException("Object properties can't be null");
            }
        }
        await _repository.Add(user);
        await _repository.Save();
    }

    public async System.Threading.Tasks.Task UpdateUser(UserDto userDto)
    {
        var user = _mapper.Map<User>(userDto);
        await _repository.Update(user);
        await _repository.Save();
    }

    public async System.Threading.Tasks.Task DeleteUser(int id)
    {
        await _repository.Delete(id);
        await _repository.Save();
    }
}
