﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
public static class CrudPrinters
{
    public async static System.Threading.Tasks.Task PrintProjects()
    {
        Console.Write("Write the ID of the project or press Enter to get all projects: ");
        var choice = Console.ReadLine();
        int.TryParse(choice, out int id);
        if (id == 0)
        {
            var projects = await CrudServices.GetAllProjects();
            foreach (var project in projects)
                Console.WriteLine($"{project.Id} {project.Name} was created on {project.CreatedAt}. It's description: {project.Description}");
            Console.WriteLine("\n");
        }
        else
        {
            var project = await CrudServices.GetProject(id);
            Console.WriteLine($"{project.Id} {project.Name} was created on {project.CreatedAt}. It's description: {project.Description}");
        }
    }

    public async static System.Threading.Tasks.Task PrintTasks()
    {
        Console.Write("Write the ID of the task or press Enter to get all tasks: ");
        var choice = Console.ReadLine();
        int.TryParse(choice, out int id);
        if (id == 0)
        {
            var tasks = await CrudServices.GetAllTasks();
            foreach (var task in tasks)
                Console.WriteLine($"{task.Id} {task.Name} was created on {task.CreatedAt}. It's {(TaskState)task.State}. It's description: {task.Description}");
            Console.WriteLine("\n");
        }
        else
        {
            var task = await CrudServices.GetTask(id);
            Console.WriteLine($"{task.Id} {task.Name} was created on {task.CreatedAt}. It's {(TaskState)task.State}. It's description: {task.Description}");
        }
    }

    public async static System.Threading.Tasks.Task PrintUsers()
    {
        Console.Write("Write the ID of the user or press Enter to get all users: ");
        var choice = Console.ReadLine();
        int.TryParse(choice, out int id);
        if (id == 0)
        {
            var users = await CrudServices.GetAllUsers();
            foreach (var user in users)
                Console.WriteLine($"{user.Id} {user.FirstName} {user.LastName} born on {user.Birthday}. Email: {user.Email}\n");
            Console.WriteLine("\n");
        }
        else
        {
            var user = await CrudServices.GetUser(id);
            Console.WriteLine($"{user.Id} {user.FirstName} {user.LastName} born on {user.Birthday}. Email: {user.Email}");
        }
    }

    public async static System.Threading.Tasks.Task PrintTeams()
    {
        Console.Write("Write the ID of the team or press Enter to get all users: ");
        var choice = Console.ReadLine();
        int.TryParse(choice, out int id);
        if (id == 0)
        {
            var teams = await CrudServices.GetAllTeams();
            foreach (var team in teams)
                Console.WriteLine($"{team.Id} {team.Name} was created on {team.CreatedAt}");
            Console.WriteLine("\n");
        }
        else
        {
            var user = await CrudServices.GetUser(id);
            Console.WriteLine($"{user.Id} {user.FirstName} {user.LastName} born on {user.Birthday}. Email: {user.Email}");
        }
    }

    // Post

    public async static System.Threading.Tasks.Task PostProject()
    {
        ProjectDto project = new ProjectDto()
        {
            CreatedAt = DateTime.Now
        };

        Console.Write("Name of project: ");
        project.Name = Console.ReadLine();

        Console.Write("Description of project: ");
        project.Description = Console.ReadLine();

        Console.Write("Author ID: ");
        int authorId = 0;
        int.TryParse(Console.ReadLine(), out authorId);
        project.AuthorId = authorId;

        Console.Write("Team ID: ");
        int teamId = 0;
        int.TryParse(Console.ReadLine(), out teamId);
        project.TeamId = teamId;

        Console.Write("Deadline of project (for ex. 23.07.2020): ");
        project.Deadline = DateTime.Parse(Console.ReadLine());

        await CrudServices.PostProject(project);
    }

    public async static System.Threading.Tasks.Task PostTask()
    {
        TaskDto task = new TaskDto()
        {
            CreatedAt = DateTime.Now,
            FinishedAt = DateTime.Now.AddDays(5),
            State = 0
        };

        Console.Write("Name of task: ");
        task.Name = Console.ReadLine();

        Console.Write("Description of task: ");
        task.Description = Console.ReadLine();

        Console.Write("Performer ID: ");
        int authorId = 0;
        int.TryParse(Console.ReadLine(), out authorId);
        task.PerformerId = authorId;

        Console.Write("Project ID: ");
        int projectId = 0;
        int.TryParse(Console.ReadLine(), out projectId);
        task.ProjectId = projectId;

        await CrudServices.PostTask(task);
    }

    public async static System.Threading.Tasks.Task PostUser()
    {
        UserDto user = new UserDto()
        {
            RegisteredAt = DateTime.Now
        };

        Console.Write("First name: ");
        user.FirstName = Console.ReadLine();

        Console.Write("Last name: ");
        user.LastName = Console.ReadLine();

        Console.Write("Birthday (for ex. 23.07.2020): ");
        user.Birthday = DateTime.Parse(Console.ReadLine());

        Console.Write("Email: ");
        user.Email = Console.ReadLine();

        Console.Write("Team ID: ");
        int teamId = 0;
        int.TryParse(Console.ReadLine(), out teamId);
        user.TeamId= teamId;

        await CrudServices.PostUser(user);
    }

    public async static System.Threading.Tasks.Task PostTeam()
    {
        TeamDto team = new TeamDto()
        {
            CreatedAt = DateTime.Now
        };

        Console.Write("Name: ");
        team.Name = Console.ReadLine();

        await CrudServices.PostTeam(team);
    }

    // Update

    public async static System.Threading.Tasks.Task UpdateProject()
    {
        ProjectDto project = new ProjectDto()
        {
            CreatedAt = DateTime.Now
        };
        Console.Write("Project ID: ");
        int id = 0;
        int.TryParse(Console.ReadLine(), out id);
        project.Id = id;

        Console.Write("Name of project: ");
        project.Name = Console.ReadLine();

        Console.Write("Description of project: ");
        project.Description = Console.ReadLine();

        Console.Write("Author ID: ");
        int authorId = 0;
        int.TryParse(Console.ReadLine(), out authorId);
        project.AuthorId = authorId;

        Console.Write("Team ID: ");
        int teamId = 0;
        int.TryParse(Console.ReadLine(), out teamId);
        project.TeamId = teamId;

        Console.Write("Deadline of project (for ex. 23.07.2020): ");
        project.Deadline = DateTime.Parse(Console.ReadLine());

        await CrudServices.UpdateProject(project);
    }

    public async static System.Threading.Tasks.Task UpdateTask()
    {
        TaskDto task = new TaskDto()
        {
            CreatedAt = DateTime.Now,
            FinishedAt = DateTime.Now.AddDays(5),
            State = 0
        };

        Console.Write("Task ID: ");
        int id = 0;
        int.TryParse(Console.ReadLine(), out id);
        task.Id = id;

        Console.Write("Name of task: ");
        task.Name = Console.ReadLine();

        Console.Write("Description of task: ");
        task.Description = Console.ReadLine();

        Console.Write("Performer ID: ");
        int authorId = 0;
        int.TryParse(Console.ReadLine(), out authorId);
        task.PerformerId = authorId;

        Console.Write("Project ID: ");
        int projectId = 0;
        int.TryParse(Console.ReadLine(), out projectId);
        task.ProjectId = projectId;

        await CrudServices.UpdateTask(task);
    }

    public async static System.Threading.Tasks.Task UpdateUser()
    {
        UserDto user = new UserDto()
        {
            RegisteredAt = DateTime.Now
        };

        Console.Write("User ID: ");
        int id = 0;
        int.TryParse(Console.ReadLine(), out id);
        user.Id = id;

        Console.Write("First name: ");
        user.FirstName = Console.ReadLine();

        Console.Write("Last name: ");
        user.LastName = Console.ReadLine();

        Console.Write("Birthday (for ex. 23.07.2020): ");
        user.Birthday = DateTime.Parse(Console.ReadLine());

        Console.Write("Email: ");
        user.Email = Console.ReadLine();

        Console.Write("Team ID: ");
        int teamId = 0;
        int.TryParse(Console.ReadLine(), out teamId);
        user.TeamId = teamId;

        await CrudServices.UpdateUser(user);
    }

    public async static System.Threading.Tasks.Task UpdateTeam()
    {
        TeamDto team = new TeamDto()
        {
            CreatedAt = DateTime.Now
        };

        Console.Write("Team ID: ");
        int id = 0;
        int.TryParse(Console.ReadLine(), out id);
        team.Id = id;

        Console.Write("Name: ");
        team.Name = Console.ReadLine();

        await CrudServices.UpdateTeam(team);
    }

    // Delete

    public async static System.Threading.Tasks.Task DeleteProject()
    {
        Console.Write("Write the ID of the project you want to delete: ");
        var choice = Console.ReadLine();
        int.TryParse(choice, out int id);

        await CrudServices.DeleteProject(id);
    }

    public async static System.Threading.Tasks.Task DeleteTask()
    {
        Console.Write("Write the ID of the task you want to delete: ");
        var choice = Console.ReadLine();
        int.TryParse(choice, out int id);

        await CrudServices.DeleteTask(id);
    }

    public async static System.Threading.Tasks.Task DeleteUser()
    {
        Console.Write("Write the ID of the user you want to delete: ");
        var choice = Console.ReadLine();
        int.TryParse(choice, out int id);

        await CrudServices.DeleteUser(id);
    }

    public async static System.Threading.Tasks.Task DeleteTeam()
    {
        Console.Write("Write the ID of the team you want to delete: ");
        var choice = Console.ReadLine();
        int.TryParse(choice, out int id);

        await CrudServices.DeleteTeam(id);
    }
}
