﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

public static class CrudServices
{
    public static HttpClient _client = new HttpClient();

    // Get all

    public static async Task<List<ProjectDto>> GetAllProjects()
    {
        var result = await _client.GetAsync("http://localhost:5001/api/projects");
        var message = await result.Content.ReadAsStringAsync();
        return JsonConvert.DeserializeObject<List<ProjectDto>>(message);
    }

    public static async Task<List<TaskDto>> GetAllTasks()
    {
        var result = await _client.GetAsync("http://localhost:5001/api/tasks");
        var message = await result.Content.ReadAsStringAsync();
        return JsonConvert.DeserializeObject<List<TaskDto>>(message);
    }

    public static async Task<List<UserDto>> GetAllUsers()
    {
        var result = await _client.GetAsync("http://localhost:5001/api/users");
        var message = await result.Content.ReadAsStringAsync();
        return JsonConvert.DeserializeObject<List<UserDto>>(message);
    }

    public static async Task<List<TeamDto>> GetAllTeams()
    {
        var result = await _client.GetAsync("http://localhost:5001/api/teams");
        var message = await result.Content.ReadAsStringAsync();
        return JsonConvert.DeserializeObject<List<TeamDto>>(message);
    }

    // Get one

    public static async Task<ProjectDto> GetProject(int id)
    {
        var result = await _client.GetAsync($"http://localhost:5001/api/projects/{id}");
        var message = await result.Content.ReadAsStringAsync();
        return JsonConvert.DeserializeObject<ProjectDto>(message);
    }

    public static async Task<TaskDto> GetTask(int id)
    {
        var result = await _client.GetAsync($"http://localhost:5001/api/tasks/{id}");
        var message = await result.Content.ReadAsStringAsync();
        return JsonConvert.DeserializeObject<TaskDto>(message);
    }

    public static async Task<UserDto> GetUser(int id)
    {
        var result = await _client.GetAsync($"http://localhost:5001/api/users/{id}");
        var message = await result.Content.ReadAsStringAsync();
        return JsonConvert.DeserializeObject<UserDto>(message);
    }

    public static async Task<TeamDto> GetTeam(int id)
    {
        var result = await _client.GetAsync($"http://localhost:5001/api/teams/{id}");
        var message = await result.Content.ReadAsStringAsync();
        return JsonConvert.DeserializeObject<TeamDto>(message);
    }

    // Post

    public static async System.Threading.Tasks.Task PostProject(ProjectDto project)
    {
        var content = JsonConvert.SerializeObject(project);
        var stringContent = new StringContent(content, UnicodeEncoding.UTF8, "application/json");
        await _client.PostAsync($"http://localhost:5001/api/projects", stringContent);
    }

    public static async System.Threading.Tasks.Task PostTask(TaskDto task)
    {
        var content = JsonConvert.SerializeObject(task);
        var stringContent = new StringContent(content, UnicodeEncoding.UTF8, "application/json");
        await _client.PostAsync($"http://localhost:5001/api/tasks", stringContent);
    }

    public static async System.Threading.Tasks.Task PostUser(UserDto user)
    {
        var content = JsonConvert.SerializeObject(user);
        var stringContent = new StringContent(content, UnicodeEncoding.UTF8, "application/json");
        await _client.PostAsync($"http://localhost:5001/api/users", stringContent);
    }

    public static async System.Threading.Tasks.Task PostTeam(TeamDto team)
    {
        var content = JsonConvert.SerializeObject(team);
        var stringContent = new StringContent(content, UnicodeEncoding.UTF8, "application/json");
        await _client.PostAsync($"http://localhost:5001/api/teams", stringContent);
    }

    // Update
    public static async System.Threading.Tasks.Task UpdateProject(ProjectDto project)
    {
        var content = JsonConvert.SerializeObject(project);
        var stringContent = new StringContent(content, UnicodeEncoding.UTF8, "application/json");
        await _client.PutAsync($"http://localhost:5001/api/projects", stringContent);
    }

    public static async System.Threading.Tasks.Task UpdateTask(TaskDto task)
    {
        var content = JsonConvert.SerializeObject(task);
        var stringContent = new StringContent(content, UnicodeEncoding.UTF8, "application/json");
        await _client.PutAsync($"http://localhost:5001/api/tasks", stringContent);
    }

    public static async System.Threading.Tasks.Task UpdateUser(UserDto user)
    {
        var content = JsonConvert.SerializeObject(user);
        var stringContent = new StringContent(content, UnicodeEncoding.UTF8, "application/json");
        await _client.PutAsync($"http://localhost:5001/api/users", stringContent);
    }

    public static async System.Threading.Tasks.Task UpdateTeam(TeamDto team)
    {
        var content = JsonConvert.SerializeObject(team);
        var stringContent = new StringContent(content, UnicodeEncoding.UTF8, "application/json");
        await _client.PutAsync($"http://localhost:5001/api/teams", stringContent);
    }

    // Delete 

    public static async System.Threading.Tasks.Task DeleteProject(int id)
    {
        var result = await _client.DeleteAsync($"http://localhost:5001/api/projects/{id}");
    }

    public static async System.Threading.Tasks.Task DeleteTask(int id)
    {
        var result = await _client.DeleteAsync($"http://localhost:5001/api/tasks/{id}");

    }

    public static async System.Threading.Tasks.Task DeleteUser(int id)
    {
        var result = await _client.DeleteAsync($"http://localhost:5001/api/users/{id}");
    }

    public static async System.Threading.Tasks.Task DeleteTeam(int id)
    {
        await _client.DeleteAsync($"http://localhost:5001/api/teams/{id}");
    }
}
