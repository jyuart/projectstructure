﻿using System;
using System.Collections.Generic;
using System.Text;

public static class LinqPrinters
{
    public async static System.Threading.Tasks.Task GetTasksInUserProjects()
    {
        Console.Write("Write ID of user: ");
        var choice = Console.ReadLine();
        int.TryParse(choice, out int id);
        var result = await LinqServices.GetTasksInUserProjects(id);
        foreach (var item in result)
            Console.WriteLine($"Project {item.Project.Name} has {item.TasksAmount} tasks");
    }

    public async static System.Threading.Tasks.Task GetAllUserTasks()
    {
        Console.Write("Write ID of user: ");
        var choice = Console.ReadLine();
        int.TryParse(choice, out int id);
        var result = await LinqServices.GetAllUserTasks(id);
        foreach (var task in result)
            Console.WriteLine($"{task.Id} {task.Name} was created on {task.CreatedAt}. It's {(TaskState)task.State}. It's description: {task.Description}");
    }

    public async static System.Threading.Tasks.Task GetAllUserTasksFinishedThisYear()
    {
        Console.Write("Write ID of user: ");
        var choice = Console.ReadLine();
        int.TryParse(choice, out int id);
        var result = await LinqServices.GetAllUserTasksFinishedThisYear(id);
        foreach (var task in result)
            Console.WriteLine($"{task.Id} {task.Name}");
    }

    public async static System.Threading.Tasks.Task GetTeamsWithUsersOlderTen()
    {
        var result = await LinqServices.GetTeamsWithUsersOlderTen();
        foreach(var item in result)
        {
            Console.WriteLine($"{item.Id} {item.Name}");
            foreach(var user in item.Users)
                Console.WriteLine($"- {user.Id} {user.FirstName} {user.LastName}");
        }
    }

    public async static System.Threading.Tasks.Task GetAllUsersWithTasks()
    {
        var result = await LinqServices.GetAllUsersWithTasks();
        foreach (var item in result)
        {
            Console.WriteLine($"{item.User.Id} {item.User.FirstName} {item.User.LastName}");
            foreach (var task in item.Tasks)
            {
                Console.WriteLine($" - {task.Id} {task.Name}");
            }
            Console.WriteLine("\n");
        }
    }

    public async static System.Threading.Tasks.Task GetUserDetails()
    {
        Console.Write("Write ID of user: ");
        var choice = Console.ReadLine();
        int.TryParse(choice, out int id);
        var result = await LinqServices.GetUserDetails(id);
        Console.WriteLine($"{result.User.Id} {result.User.FirstName} {result.User.LastName}");
        Console.WriteLine($"Last project: {result.LastProject.Name} has {result.ProjectTasks} tasks");
        Console.WriteLine($"Longest task: {result.LongestTask.Name}");
        Console.WriteLine($"User has {result.NotFinishedTasks} unfinished tasks");
    }

    public async static System.Threading.Tasks.Task GetProjectsDetails()
    {
        var result = await LinqServices.GetProjectsDetails();
        foreach(var item in result)
        {
            Console.WriteLine($"{item.Project.Id} {item.Project.Name}. It has {item.Members} members in team.");
            Console.WriteLine($"Longest task: {item.LongestTask.Id} {item.LongestTask.Name}");
            Console.WriteLine($"Shortest task: {item.ShortestTask.Id} {item.ShortestTask.Name}\n");
        }
    }

    public async static System.Threading.Tasks.Task GetNotFinishedTask()
    {
        Console.Write("Write ID of user: ");
        var choice = Console.ReadLine();
        int.TryParse(choice, out int id);
        var result = await LinqServices.GetNotFinishedTasks(id);
        foreach(var task in result)
            Console.WriteLine($"{task.Id} {task.Name} was created on {task.CreatedAt}. It's {(TaskState)task.State}. It's description: {task.Description}");
    }
}