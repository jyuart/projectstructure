﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

public static class LinqServices
{
    public static HttpClient _client = new HttpClient();
    public static async Task<List<TasksPerUser>> GetTasksInUserProjects(int id)
    {
        var result = await _client.GetAsync($"http://localhost:5001/api/projects/TasksPerUser/{id}");
        var message = await result.Content.ReadAsStringAsync();
        return JsonConvert.DeserializeObject<List<TasksPerUser>>(message);
    }

    public static async Task<List<TaskDto>> GetAllUserTasks(int id)
    {
        var result = await _client.GetAsync($"http://localhost:5001/api/tasks/peruser/{id}");
        var message = await result.Content.ReadAsStringAsync();
        return JsonConvert.DeserializeObject<List<TaskDto>>(message);
    }

    public static async Task<List<FinishedThisYear>> GetAllUserTasksFinishedThisYear(int id)
    {
        var result = await _client.GetAsync($"http://localhost:5001/api/tasks/FinishedThisYearPerUser/{id}");
        var message = await result.Content.ReadAsStringAsync();
        return JsonConvert.DeserializeObject<List<FinishedThisYear>>(message);
    }

    public static async Task<List<TeamWithUsersOlderTen>> GetTeamsWithUsersOlderTen()
    {
        var result = await _client.GetAsync($"http://localhost:5001/api/teams/WithUsersOlderTen");
        var message = await result.Content.ReadAsStringAsync();
        return JsonConvert.DeserializeObject<List<TeamWithUsersOlderTen>>(message);
    }

    public static async Task<List<UserTasks>> GetAllUsersWithTasks()
    {
        var result = await _client.GetAsync($"http://localhost:5001/api/users/WithTasks");
        var message = await result.Content.ReadAsStringAsync();
        return JsonConvert.DeserializeObject<List<UserTasks>>(message);
    }

    public static async Task<UserDetails> GetUserDetails(int id)
    {
        var result = await _client.GetAsync($"http://localhost:5001/api/users/details/{id}");
        var message = await result.Content.ReadAsStringAsync();
        return JsonConvert.DeserializeObject<UserDetails>(message);
    }

    public static async Task<List<ProjectDetails>> GetProjectsDetails()
    {
        var result = await _client.GetAsync($"http://localhost:5001/api/projects/details");
        var message = await result.Content.ReadAsStringAsync();
        return JsonConvert.DeserializeObject<List<ProjectDetails>>(message);
    }

    public static async Task<List<TaskDto>> GetNotFinishedTasks(int id)
    {
        var result = await _client.GetAsync($"http://localhost:5001/api/tasks/proceeding/{id}");
        var message = await result.Content.ReadAsStringAsync();
        return JsonConvert.DeserializeObject<List<TaskDto>>(message);
    }
}
