﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

public static class Program
{
    private static readonly HttpClient client = new HttpClient();
    public static async System.Threading.Tasks.Task Main(string[] args)
    {
        string choice = "";
        do {
            choice = Options();
            switch(choice)
            {
                case "0":
                    Parallel.Invoke(async() => await queries.MarkRandomTaskWithDelay(1000));
                    Wait();
                    break;
                case "1":
                    await CrudPrinters.PrintProjects();
                    Wait();
                    break;
                case "2":
                    await CrudPrinters.PrintTasks();
                    Wait();
                    break;
                case "3":
                    await CrudPrinters.PrintUsers();
                    Wait();
                    break;
                case "4":
                    await CrudPrinters.PrintTeams();
                    Wait();
                    break;
                case "5":
                    await CrudPrinters.PostProject();
                    Wait();
                    break;
                case "6":
                    await CrudPrinters.PostTask();
                    Wait();
                    break;
                case "7":
                    await CrudPrinters.PostUser();
                    Wait();
                    break;
                case "8":
                    await CrudPrinters.PostTeam();
                    Wait();
                    break;
                case "9":
                    await CrudPrinters.UpdateProject();
                    Wait();
                    break;
                case "10":
                    await CrudPrinters.UpdateTask();
                    Wait();
                    break;
                case "11":
                    await CrudPrinters.UpdateUser();
                    Wait();
                    break;
                case "12":
                    await CrudPrinters.UpdateTeam();
                    Wait();
                    break;
                case "13":
                    await CrudPrinters.DeleteProject();
                    Wait();
                    break;
                case "14":
                    await CrudPrinters.DeleteTask();
                    Wait();
                    break;
                case "15":
                    await CrudPrinters.DeleteUser();
                    Wait();
                    break;
                case "16":
                    await CrudPrinters.DeleteTeam();
                    Wait();
                    break;
                case "17":
                    await LinqPrinters.GetTasksInUserProjects();
                    Wait();
                    break;
                case "18":
                    await LinqPrinters.GetAllUserTasks();
                    Wait();
                    break;
                case "19":
                    await LinqPrinters.GetAllUserTasksFinishedThisYear();
                    Wait();
                    break;
                case "20":
                    await LinqPrinters.GetTeamsWithUsersOlderTen();
                    Wait();
                    break;
                case "21":
                    await LinqPrinters.GetAllUsersWithTasks();
                    Wait();
                    break;
                case "22":
                    await LinqPrinters.GetUserDetails();
                    Wait();
                    break;
                case "23":
                    await LinqPrinters.GetProjectsDetails();
                    Wait();
                    break;
                case "24":
                    await LinqPrinters.GetNotFinishedTask();
                    Wait();
                    break;
            }
        } while (choice != "q"); 
    }

    public static string Options()
    {
        Console.WriteLine("What do you want to do? (Enter q to exit)");
        Console.WriteLine("0. Mark one task as finished (will be done in background)");
        Console.WriteLine("1. Get projects");
        Console.WriteLine("2. Get tasks");
        Console.WriteLine("3. Get users");
        Console.WriteLine("4. Get teams\n");
        Console.WriteLine("5. Post project");
        Console.WriteLine("6. Post task");
        Console.WriteLine("7. Post user");
        Console.WriteLine("8. Post team\n");
        Console.WriteLine("9. Update project");
        Console.WriteLine("10. Update task");
        Console.WriteLine("11. Update user");
        Console.WriteLine("12. Update team\n");
        Console.WriteLine("13. Delete project");
        Console.WriteLine("14. Delete task");
        Console.WriteLine("15. Delete user");
        Console.WriteLine("16. Delete team\n");
        Console.WriteLine("17. Get tasks for all user's projects");
        Console.WriteLine("18. Get all user's tasks with name < 45");
        Console.WriteLine("19. Get all user's tasks finished this year");
        Console.WriteLine("20. Get all teams with members older than 10");
        Console.WriteLine("21. Get all users with their tasks");
        Console.WriteLine("22. Get user details");
        Console.WriteLine("23. Get all projects with details");
        Console.WriteLine("24. Get all user's unfinished task");

        Console.Write("\nYour choice: ");
        return Console.ReadLine();
    }

    public static void Wait()
    {
        Console.WriteLine("\n***");
        Console.WriteLine("\nPress Enter");
        Console.ReadLine();
    }
}
