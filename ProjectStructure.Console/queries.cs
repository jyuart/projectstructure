﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

public static class queries
{
    public static TaskCompletionSource<int> completionSource = new TaskCompletionSource<int>();
    public static Task<int> MarkRandomTaskWithDelay(int interval)
    {
        Timer timer = new Timer(interval)
        {
            AutoReset = false
        };

        timer.Elapsed += MarkingTask;

        timer.Start();
        return completionSource.Task;
    }

    private async static void MarkingTask(object o, ElapsedEventArgs e)
    {
        try
        {
            var random = new Random();

            var result = await CrudServices.GetAllTasks();

            var taskNo = random.Next(result.Count);
            var task = result[taskNo];

            task.State = (int)TaskState.Finished;
            await CrudServices.UpdateTask(task);

            Console.WriteLine($"Task with ID {task.Id} was marked as 'finished' in background");

            completionSource.SetResult(task.Id);
        }
        catch (Exception ex)
        {
            completionSource.SetException(ex);
        }
    }
}
