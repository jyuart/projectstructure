using System.Collections.Generic;
using System.Threading.Tasks;

public interface IRepository<T> where T: class
{
    Task<List<T>> GetAll();
    Task<T> Get(int id);
    System.Threading.Tasks.Task Add(T entity);
    System.Threading.Tasks.Task Update(T entity);
    System.Threading.Tasks.Task Delete(int id);
    System.Threading.Tasks.Task Save();
}