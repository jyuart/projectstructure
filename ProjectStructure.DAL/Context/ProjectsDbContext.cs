using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;


public class ProjectsDbContext : DbContext
{
    public ProjectsDbContext(DbContextOptions<ProjectsDbContext> options) : base(options) {}
    public DbSet<Project> Projects {get; set;}
    public DbSet<Task> Tasks {get; set;}
    public DbSet<User> Users {get; set;}
    public DbSet<Team> Teams {get; set;}

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);


        modelBuilder
            .Entity<Project>().Property(x => x.Id).HasColumnName("ProjectId");
        modelBuilder
            .Entity<Task>().Property(x => x.Id).HasColumnName("TaskId");
        modelBuilder
            .Entity<User>().Property(x => x.Id).HasColumnName("UserId");
        modelBuilder
            .Entity<Team>().Property(x => x.Id).HasColumnName("TeamId");
        
        var mockProjects = JsonConvert.DeserializeObject<List<Project>>(File.ReadAllText(@"C:\Users\yurap\bsa20\project-structure\ProjectStructure.DAL\MockData\MockProjects.json"));
        var mockTasks = JsonConvert.DeserializeObject<List<Task>>(File.ReadAllText(@"C:\Users\yurap\bsa20\project-structure\ProjectStructure.DAL\MockData\MockTasks.json"));
        var mockUsers = JsonConvert.DeserializeObject<List<User>>(File.ReadAllText(@"C:\Users\yurap\bsa20\project-structure\ProjectStructure.DAL\MockData\MockUsers.json"));
        var mockTeams = JsonConvert.DeserializeObject<List<Team>>(File.ReadAllText(@"C:\Users\yurap\bsa20\project-structure\ProjectStructure.DAL\MockData\MockTeams.json"));
        
        modelBuilder.Entity<Team>().HasData(mockTeams);
        modelBuilder.Entity<User>().HasData(mockUsers);
        modelBuilder.Entity<Project>().HasData(mockProjects);
        modelBuilder.Entity<Task>().HasData(mockTasks); 
    }
}