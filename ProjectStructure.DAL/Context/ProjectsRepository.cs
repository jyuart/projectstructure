using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

public class ProjectsRepository : IRepository<Project>
{
    private readonly ProjectsDbContext _context;

    public ProjectsRepository(ProjectsDbContext dbContext)
    {
        _context = dbContext;
    }

    public Task<List<Project>> GetAll()
    {
        return _context.Projects.ToListAsync();
    }

    public Task<Project> Get(int id)
    {
        return _context.Projects.FirstOrDefaultAsync(p => p.Id == id);
    }

    public async System.Threading.Tasks.Task Add(Project project)
    {
        await _context.Projects.AddAsync(project);
    }

    public async System.Threading.Tasks.Task Update(Project project)
    {
        var entry = await _context.Projects.FirstOrDefaultAsync(p => p.Id == project.Id);
        _context.Entry(entry).CurrentValues.SetValues(project);
    }

    public async System.Threading.Tasks.Task Delete(int id)
    {
        var toDelete = await _context.Projects.FirstOrDefaultAsync(p => p.Id == id);
        _context.Projects.Remove(toDelete);
    }

    public async System.Threading.Tasks.Task Save()
    {
        await _context.SaveChangesAsync();
    }
}