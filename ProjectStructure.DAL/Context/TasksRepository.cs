using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

public class TasksRepository : IRepository<Task>
{
    private readonly ProjectsDbContext _context;
    public TasksRepository(ProjectsDbContext dbContext)
    {
        _context = dbContext;
    }

    public Task<List<Task>> GetAll()
    {
        return _context.Tasks.ToListAsync();
    }

    public Task<Task> Get(int id)
    {
        return _context.Tasks.FirstOrDefaultAsync(t => t.Id == id);
    }

    public async System.Threading.Tasks.Task Add(Task task)
    {
        await _context.Tasks.AddAsync(task);
    }

    public async System.Threading.Tasks.Task Update(Task task)
    {
        var entry = await _context.Tasks.FirstOrDefaultAsync(t => t.Id == task.Id);
        _context.Entry(entry).CurrentValues.SetValues(task);
    }

    public async System.Threading.Tasks.Task Delete(int id)
    {
        var toDelete = await _context.Tasks.FirstOrDefaultAsync(t => t.Id == id);
        _context.Tasks.Remove(toDelete);
    }

    public async System.Threading.Tasks.Task Save()
    {
        await _context.SaveChangesAsync();
    }
}