using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

public class TeamsRepository : IRepository<Team>
{
    private readonly ProjectsDbContext _context;

    public TeamsRepository(ProjectsDbContext dbContext)
    {
        _context = dbContext;
    }

    public Task<List<Team>> GetAll()
    {
        return _context.Teams.ToListAsync();
    }

    public Task<Team> Get(int id)
    {
        return _context.Teams.FirstOrDefaultAsync(t => t.Id == id);
    }

    public async System.Threading.Tasks.Task Add(Team team)
    {
        await _context.Teams.AddAsync(team);
    }

    public async System.Threading.Tasks.Task Update(Team team)
    {
        var entry = await _context.Teams.FirstOrDefaultAsync(t => t.Id == team.Id);
        _context.Entry(entry).CurrentValues.SetValues(team);
    }

    public async System.Threading.Tasks.Task Delete(int id)
    {
        var toDelete = await _context.Teams.FirstOrDefaultAsync(t => t.Id == id);
        _context.Teams.Remove(toDelete);
    }

    public async System.Threading.Tasks.Task Save()
    {
        await _context.SaveChangesAsync();
    }
}