using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

public class UsersRepository : IRepository<User>
{
    private readonly ProjectsDbContext _context;
    public UsersRepository(ProjectsDbContext dbContext)
    {
        _context = dbContext;
    }

    public Task<List<User>> GetAll()
    {
        return _context.Users.ToListAsync();
    }

    public Task<User> Get(int id)
    {
        return _context.Users.FirstOrDefaultAsync(t => t.Id == id);
    }

    public async System.Threading.Tasks.Task Add(User user)
    {
        await _context.Users.AddAsync(user);
    }

    public async System.Threading.Tasks.Task Update(User user)
    {
        var entry = await _context.Users.FirstOrDefaultAsync(u => u.Id == user.Id);
        _context.Entry(entry).CurrentValues.SetValues(user);
    }
    public async System.Threading.Tasks.Task Delete(int id)
    {
        var toDelete = await _context.Users.FirstOrDefaultAsync(u => u.Id == id);
        _context.Users.Remove(toDelete);
    }

    public async System.Threading.Tasks.Task Save()
    {
        await _context.SaveChangesAsync();
    }
}