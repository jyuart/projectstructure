public class ProjectDetails {
    public ProjectDto Project {get; set;}
    public TaskDto LongestTask {get; set;}
    public TaskDto ShortestTask {get; set;}
    public int Members {get; set;}
}