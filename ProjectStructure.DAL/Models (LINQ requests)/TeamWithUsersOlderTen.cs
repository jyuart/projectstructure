using System.Collections.Generic;

public class TeamWithUsersOlderTen
{
    public int Id {get; set;}
    public string Name {get; set;}
    public List<UserDto> Users {get; set;}
}