public class UserDetails
{
    public UserDto User { get; set; }
    public ProjectDto LastProject { get; set; }
    public int ProjectTasks { get; set; }
    public int NotFinishedTasks { get; set; }
    public TaskDto LongestTask { get; set; }
}