using System.Collections.Generic;

public class UserTasks
{
    public UserDto User {get; set;}
    public List<TaskDto> Tasks {get; set;}
}