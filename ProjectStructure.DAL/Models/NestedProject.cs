﻿using System;
using System.Collections.Generic;

public class NestedProject : Project
{
    public List<NestedTask> Tasks { get; set; }
    public User Author { get; set; }
    public Team Team { get; set; }
}