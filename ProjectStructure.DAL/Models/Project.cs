using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

public class Project {
    [Required]
    [Key]
    public int Id {get; set;}
    [Required]
    public string Name {get; set;}
    [Required]
    public string Description {get; set;}
    [DataType(DataType.Date)]
    public DateTime CreatedAt {get; set;}
    [DataType(DataType.Date)]
    public DateTime Deadline {get; set;}
    [Required]
    public int AuthorId {get; set;}
    [Required]
    public int TeamId {get; set;}
}