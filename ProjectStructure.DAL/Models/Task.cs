using System;
using System.ComponentModel.DataAnnotations;

public class Task
{
    [Required]
    [Key]
    public int Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    [DataType(DataType.Date)]
    public DateTime CreatedAt { get; set; }
    [DataType(DataType.Date)]
    public DateTime FinishedAt { get; set; }
    public int State { get; set; }
    [Required]
    public int ProjectId { get; set; }
    [Required]
    public int PerformerId { get; set; }
}