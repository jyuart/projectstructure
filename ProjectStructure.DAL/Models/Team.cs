using System;
using System.ComponentModel.DataAnnotations;

public class Team 
{
    public Team()
    {
        CreatedAt = DateTime.UtcNow;
    }
    [Required]
    [Key]
    public int Id {get; set;}
    public string Name {get; set;}
    [DataType(DataType.Date)]
    public DateTime CreatedAt {get; set;}
}