using System;
using System.ComponentModel.DataAnnotations;

public class User 
{
    [Required]
    [Key]
    public int Id {get; set;}
    public string FirstName {get; set;}
    public string LastName {get; set;}
    [DataType(DataType.EmailAddress)]
    public string Email {get; set;}
    [DataType(DataType.Date)]
    public DateTime Birthday {get; set;}
    [DataType(DataType.Date)]
    public DateTime RegisteredAt {get; set;}
    public int ?TeamId {get; set;}
}