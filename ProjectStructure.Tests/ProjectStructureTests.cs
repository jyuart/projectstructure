using System;
using Xunit;
using Microsoft.EntityFrameworkCore;
using FakeItEasy;
using System.Linq;
using AutoMapper;
using System.Threading.Tasks;

public class ProjectStructureTests
{
    private DbContextOptions<ProjectsDbContext> options;
    private UsersRepository _usersRepository;
    private TasksRepository _tasksRepository;
    private TeamsRepository _teamsRepository;
    private ProjectsRepository _projectsRepository;
    private UserService _userService;
    private ProjectService _projectService;
    private TaskService _taskService;
    private TeamService _teamService;
    private Service _service;
    private IMapper _mapper;
    public ProjectStructureTests()
    {
        // Create new InMemoryDB for each test
        var builder = new DbContextOptionsBuilder<ProjectsDbContext>();
        builder.UseInMemoryDatabase("DB for test");
        options = builder.Options;
        ProjectsDbContext context = new ProjectsDbContext(options);

        // Delete and create DB before each test
        context.Database.EnsureDeleted();
        context.Database.EnsureCreated();

        // Services for testing business logic
        _usersRepository = new UsersRepository(context);
        _tasksRepository = new TasksRepository(context);
        _teamsRepository = new TeamsRepository(context);
        _projectsRepository = new ProjectsRepository(context);

        var mappingConfig = new MapperConfiguration(mc =>
        {
            mc.AddProfile(new UserProfile());
            mc.AddProfile(new ProjectProfile());
            mc.AddProfile(new TaskProfile());
            mc.AddProfile(new TeamProfile());
        });
        IMapper mapper = mappingConfig.CreateMapper();
        _mapper = mapper;


        _userService = new UserService(_usersRepository, _mapper);
        _taskService = new TaskService(_tasksRepository, _mapper);
        _projectService = new ProjectService(_projectsRepository, _mapper);
        _teamService = new TeamService(_teamsRepository, _mapper);

    }

    [Fact]
    public async System.Threading.Tasks.Task CreateUser_WhenNewUser_ThenUsersPlusOne()
    {
        UserDto user = new UserDto()
        {
            FirstName = "Elon",
            LastName = "Musk",
            Birthday = DateTime.Now,
            RegisteredAt = DateTime.Now,
            TeamId = 1,
            Email = "elon@musk.com"
        };

        await _userService.PostUser(user);

        var result = await _userService.GetAllUsers();
        Assert.Equal(51, result.Count);
    }

    [Fact]
    public async System.Threading.Tasks.Task CreateUser_ThenPostUserMethodInvoked()
    {
        var fakeRepository = A.Dummy<IRepository<User>>();
        var fakeService = new UserService(fakeRepository, _mapper);
        UserDto userDto = new UserDto()
        {
            FirstName = "Elon",
            LastName = "Musk",
            Birthday = DateTime.Now,
            RegisteredAt = DateTime.Now,
            TeamId = 1,
            Email = "elon@musk.com"
        };

        await fakeService.PostUser(userDto);

        A.CallTo(() => fakeRepository.Add(A<User>.That.Matches(u =>
            u.FirstName == userDto.FirstName))).MustHaveHappened();
    }

    [Fact]
    public async System.Threading.Tasks.Task CreateUser_WhenUserFieldsEmpty_ThenThrowArgumentNullException()
    {
        UserDto user = new UserDto();

        await Assert.ThrowsAsync<ArgumentException>(() => _userService.PostUser(user));
    }

    [Fact]
    public async System.Threading.Tasks.Task ChangeTaskState_ThenTaskStateFinished()
    {
        TaskDto task = new TaskDto()
        {
            Name = "Random task",
            Description = "Random task description",
            CreatedAt = DateTime.Now,
            FinishedAt = DateTime.Now,
            PerformerId = 1,
            ProjectId = 1,
            State = 0
        };
        await _taskService.PostTask(task);
        task.Id = 201;
        task.State = (int)TaskState.Finished;

        await _taskService.UpdateTask(task);

        TaskDto changedTask = await _taskService.GetTask(201);
        Assert.Equal(TaskState.Finished, (TaskState)changedTask.State);
    }

    [Fact]
    public async System.Threading.Tasks.Task ChangeTaskState_ThenUpdateTaskMethodInvoked()
    {
        TaskDto taskDto = new TaskDto();
        var fakeRepository = A.Fake<IRepository<Task>>();
        var fakeService = new TaskService(fakeRepository, _mapper);

        await fakeService.UpdateTask(taskDto);
        A.CallTo(() => fakeRepository.Update(A<Task>.That.Matches(t =>
            t.FinishedAt == taskDto.FinishedAt))).MustHaveHappened();
    }

    [Fact]
    public async System.Threading.Tasks.Task AddUserToTeam1_ThenUserTeamId1()
    {
        UserDto user = new UserDto()
        {
            FirstName = "Elon",
            LastName = "Musk",
            Birthday = DateTime.Now,
            RegisteredAt = DateTime.Now,
            TeamId = 1,
            Email = "elon@musk.com"
        };

        await _userService.PostUser(user);

        UserDto getUser = await _userService.GetUser(51);
        Assert.Equal(1, getUser.TeamId);
    }

    [Fact]
    public async System.Threading.Tasks.Task AddUserToTeam1_ThenUpadeUserMethodInvoke()
    {
        UserDto userDto = new UserDto();
        var fakeRepository = A.Fake<IRepository<User>>();
        var fakeService = new UserService(fakeRepository, _mapper);

        await fakeService.UpdateUser(userDto);

        A.CallTo(() => fakeRepository.Update(A<User>.That.Matches(u =>
            u.Birthday == userDto.Birthday))).MustHaveHappened();
    }

    [Fact]
    public async System.Threading.Tasks.Task GetUserTasks_WhenNoTasks_ThenNoContent()
    {
        _service = new Service(_projectsRepository, _tasksRepository, _usersRepository, _teamsRepository, A.Fake<IMapper>());
        var result = await _service.GetTasksInUserProjects(2);

        Assert.Empty(result);
    }

    [Fact]
    public async System.Threading.Tasks.Task GetUserTasksWithNamesLess45_NoTasksWithNameMore45()
    {
        _service = new Service(_projectsRepository, _tasksRepository, _usersRepository, _teamsRepository, _mapper);

        var result = await _service.GetAllUserTasks(7);
        Assert.Equal(3, result.Count);
        foreach (var item in result)
            Assert.InRange(item.Name.Length, 1, 45);
    }

    [Fact]
    public async System.Threading.Tasks.Task GetUserTasksFinishedThisYear()
    {
        _service = new Service(_projectsRepository, _tasksRepository, _usersRepository, _teamsRepository, A.Fake<IMapper>());
        var result = await _service.GetAllUserTasksFinishedThisYear(1);
        Assert.Single(result);
    }

    [Fact]
    public async System.Threading.Tasks.Task GetTeamsWithUsersOlder10_ThenAllUsersBirtdayYearLess2010()
    {
        _service = new Service(_projectsRepository, _tasksRepository, _usersRepository, _teamsRepository, _mapper);

        var result = await _service.GetTeamsWithUsersOlderTen();

        foreach (var item in result)
            foreach (var user in item.Users)
                Assert.InRange(user.Birthday.Year, 1900, 2010);
    }

    [Fact]
    public async System.Threading.Tasks.Task GetUserSortedAlphabetically()
    {
        _service = new Service(_projectsRepository, _tasksRepository, _usersRepository, _teamsRepository, _mapper);

        var result = await _service.GetAllUsersWithTasks();
        var orderedResult = result.OrderBy(x => x.User.FirstName);

        Assert.Equal(orderedResult, result);
    }

    [Fact]
    public async System.Threading.Tasks.Task GetUserDetails()
    {
        _service = new Service(_projectsRepository, _tasksRepository, _usersRepository, _teamsRepository, _mapper);

        var result = await _service.GetUserDetails(1);
        Assert.IsType<ProjectDto>(result.LastProject);
        Assert.IsType<TaskDto>(result.LongestTask);
    }

    [Fact]
    public async System.Threading.Tasks.Task GetProjectDetails()
    {
        _service = new Service(_projectsRepository, _tasksRepository, _usersRepository, _teamsRepository, _mapper);

        var result = await _service.GetProjectsDetails();
        foreach (var item in result)
        {
            Assert.IsType<ProjectDto>(item.Project);
            Assert.IsType<TaskDto>(item.LongestTask);
            Assert.IsType<TaskDto>(item.ShortestTask);
        }
    }


    // Tests for new endpoint

    [Fact]
    public async System.Threading.Tasks.Task GetNotFinishedUserTasks_WhenTasksExist_ThenTasksStateNotFinished()
    {
        _service = new Service(_projectsRepository, _tasksRepository, _usersRepository, _teamsRepository, _mapper);

        var result = await _service.GetNotFinishedTasks(7);
        foreach (var task in result)
            Assert.NotEqual(TaskState.Finished, (TaskState)task.State);
    }

    [Fact]
    public async System.Threading.Tasks.Task GetNotFinishedUserTasks_WhenNoTasks()
    {
        UserDto userDto = new UserDto()
        {
            FirstName = "Name",
            LastName = "LastName",
            Birthday = DateTime.Now,
            RegisteredAt = DateTime.Now,
            Email = "n@n.com",
            TeamId = null
        };
        await _userService.PostUser(userDto);

        _service = new Service(_projectsRepository, _tasksRepository, _usersRepository, _teamsRepository, _mapper);
        var result = await _service.GetNotFinishedTasks(51);

        Assert.Empty(result);
    }
}