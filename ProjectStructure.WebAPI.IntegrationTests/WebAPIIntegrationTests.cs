using FakeItEasy;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using ProjectStructure.WebAPI;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices;
using Xunit;

public class WebAPIIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
{
    private readonly CustomWebApplicationFactory<Startup> _factory;
    private readonly HttpClient _client;

    public WebAPIIntegrationTests(CustomWebApplicationFactory<Startup> factory)
    {
        _factory = factory;
        _client = _factory.CreateDefaultClient();
    }

    [Fact]
    public async void CreateProject_ThenNewProjectInDb()
    {
        var project = new Project()
        {
            Name = "Newest testing project",
            Description = "Testing project description",
            CreatedAt = DateTime.Now,
            Deadline = DateTime.Now,
            AuthorId = 1,
            TeamId = 1
        };
        var jsonProject = JsonConvert.SerializeObject(project);
        var content = new ByteArrayContent(System.Text.Encoding.UTF8.GetBytes(jsonProject));
        content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

        var post = await _client.PostAsync("api/projects", content);
        var response = await _client.GetAsync("api/projects/101");

        var responseContent = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
        var responseProject = JsonConvert.DeserializeObject<ProjectDto>(responseContent);

        Assert.Equal(project.Name, responseProject.Name);
    }

    [Fact]
    public async void CreateProjectWithEmptyProperties_ThenError400()
    {
        var project = new Project();
        var jsonProject = JsonConvert.SerializeObject(project);
        var content = new ByteArrayContent(System.Text.Encoding.UTF8.GetBytes(jsonProject));
        content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

        var post = await _client.PostAsync("api/projects", content);
        Assert.Equal(HttpStatusCode.BadRequest, post.StatusCode);
    }

    [Fact]
    public async void DeleteUser_ThenUsersInCollection49()
    {
        var firstResponse = await _client.GetAsync("api/users");
        var collectionBefore = JsonConvert.DeserializeObject<List<UserDto>>(firstResponse.Content.ReadAsStringAsync().GetAwaiter().GetResult());
        await _client.DeleteAsync("api/users/50");

        var secondResponse = await _client.GetAsync("api/users");
        var collectionAfter = JsonConvert.DeserializeObject<List<UserDto>>(secondResponse.Content.ReadAsStringAsync().GetAwaiter().GetResult());

        Assert.NotEqual(collectionBefore.Count, collectionAfter.Count);
    }

    [Fact]
    public async void DeleteUser_WhenUserNotExist_ThenReturnBadRequest()
    {
        var response = await _client.DeleteAsync("api/users/100");
        Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
    }

    [Fact]
    public async void CreateTeam_ThenNewTeamInDb()
    {
        var team = new Team()
        {
            Name = "Testing team",
            CreatedAt = DateTime.Now
        };
        var jsonTeam = JsonConvert.SerializeObject(team);
        var content = new ByteArrayContent(System.Text.Encoding.UTF8.GetBytes(jsonTeam));
        content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

        var post = await _client.PostAsync("api/teams", content);
        var response = await _client.GetAsync("api/teams/11");

        var responseContent = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
        var responseTeam = JsonConvert.DeserializeObject<TeamDto>(responseContent);

        Assert.Equal(team.Name, responseTeam.Name);
    }

    [Fact]
    public async void CreateTeamWithEmptyProperties_ThenError400()
    {
        var team = new Team();
        var jsonTeam = JsonConvert.SerializeObject(team);
        var content = new ByteArrayContent(System.Text.Encoding.UTF8.GetBytes(jsonTeam));
        content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

        var post = await _client.PostAsync("api/teams", content);
        Assert.Equal(HttpStatusCode.BadRequest, post.StatusCode);
    }

    [Fact]
    public async void DeleteTask_ThenTasksInCollection199()
    {
        await _client.DeleteAsync("api/tasks/200");

        var response = await _client.GetAsync("api/tasks");
        var responeCollection = JsonConvert.DeserializeObject<List<TaskDto>>(response.Content.ReadAsStringAsync().GetAwaiter().GetResult());

        Assert.Equal(199, responeCollection.Count);
    }

    [Fact]
    public async void DeleteTask_WhenTaskNotExist_ThenReturnBadRequest()
    {
        var response = await _client.DeleteAsync("api/tasks/300");
        Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
    }

    [Fact]
    public async void GetStructureWithProjectDetails_ThenGetCollectionOfProjects()
    {
        var response = await _client.GetAsync("api/projects/details");
        var result = JsonConvert.DeserializeObject<List<ProjectDetails>>(response.Content.ReadAsStringAsync().GetAwaiter().GetResult());

        Assert.Equal(89, result.Count);
    }

    [Fact]
    public async void GetStructureWithProjectDetails_ThenEachItemOfSpecifcType()
    {
        var response = await _client.GetAsync("api/projects/details");
        var result = JsonConvert.DeserializeObject<List<ProjectDetails>>(response.Content.ReadAsStringAsync().GetAwaiter().GetResult());

        foreach (var item in result)
        {
            Assert.IsType<ProjectDto>(item.Project);
            Assert.IsType<TaskDto>(item.LongestTask);
            Assert.IsType<TaskDto>(item.ShortestTask);
            Assert.IsType<int>(item.Members);
        }
    }

    // Tests for new endpoint

    [Fact]
    public async void GetNotFinishedUserTasks_WhenTasksExist_ThenTasksStateNotFinished()
    {
        var response = await _client.GetAsync("api/tasks/proceeding/7");
        var result = JsonConvert.DeserializeObject<List<TaskDto>>(response.Content.ReadAsStringAsync().GetAwaiter().GetResult());

        foreach (var item in result)
            Assert.NotEqual(TaskState.Finished, (TaskState)item.State);
    }

    [Fact]
    public async void GetNotFinishedUserTasks_WhenNoTasks()
    {
        User user = new User()
        {
            FirstName = "Elon",
            LastName = "Musk",
            Birthday = DateTime.Now,
            RegisteredAt = DateTime.Now,
            TeamId = 1,
            Email = "elon@musk.com"
        };
        var jsonUser = JsonConvert.SerializeObject(user);
        var content = new ByteArrayContent(System.Text.Encoding.UTF8.GetBytes(jsonUser));
        content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

        await _client.PostAsync("api/users", content);

        var response = await _client.GetAsync("api/tasks/proceeding/51");

        Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
    }
}