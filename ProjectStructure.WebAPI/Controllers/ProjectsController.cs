using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using System.Linq;

[Route("api/[controller]")]
[ApiController]
public class ProjectsController : ControllerBase 
{
    private readonly ProjectService _projectService;
    private readonly Service _service;

    public ProjectsController(ProjectService projectService, Service service) 
    {
        _service = service;
        _projectService = projectService;
    }

    [HttpGet]
    public async Task<ActionResult<List<ProjectDto>>> GetAllProjects()
    {
        return await _projectService.GetAllProjects();
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<ProjectDto>> GetProject(int id)
    {
        return await _projectService.GetProject(id);
    }

    [HttpPost]
    public async Task<ActionResult<ProjectDto>> PostProject(ProjectDto project)
    {
        try
        {
            await _projectService.PostProject(project);
            return project;
        }
        catch
        {
            return BadRequest();
        }
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult> DeleteProject(int id)
    {
        try
        {
            await _projectService.DeleteProject(id);
            return NoContent();
        }
        catch
        {
            return BadRequest();
        }
    }

    [HttpPut]
    public async Task<ActionResult> UpdateProject(ProjectDto project)
    {
        await _projectService.UpdateProject(project);
        return NoContent();
    }

    [HttpGet("TasksPerUser/{id}")]
    public async Task<ActionResult<List<TasksPerUser>>> GetTasksInUserProjects(int id)
    {
        var result = await _service.GetTasksInUserProjects(id);
        if (result.Any())
            return result;
        else 
            return NotFound();
    }

    [HttpGet("Details")]
    public async Task<ActionResult<List<ProjectDetails>>> GetAllProjectsDetails()
    {
        return await _service.GetProjectsDetails();
    }
}