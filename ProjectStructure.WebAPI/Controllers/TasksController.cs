using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using System.Linq;
using System.Threading.Tasks;

[Route("api/[controller]")]
[ApiController]
public class TasksController : ControllerBase 
{
    private readonly TaskService _taskService;
    private readonly Service _service;

    public TasksController(TaskService taskService, Service service) 
    {
        _taskService = taskService;
        _service = service;
    }

    [HttpGet]
    public async Task<ActionResult<List<TaskDto>>> GetAllTasks()
    {
        return await _taskService.GetAllTasks();
    }
    

    [HttpGet("{id}")]
    public async Task<ActionResult<TaskDto>> GetTask(int id)
    {
        return await _taskService.GetTask(id);
    }

    [HttpPost]
    public async Task<ActionResult<TaskDto>> PostTask(TaskDto task)
    {
        try
        {
            await _taskService.PostTask(task);
            return task;
        }
        catch
        {
            return BadRequest();
        }
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult> DeleteTask(int id)
    {
        try
        {
            await _taskService.DeleteTask(id);
            return NoContent();
        }
        catch
        {
            return BadRequest();
        }
    }

    [HttpPut]
    public async Task<ActionResult> UpdateTask(TaskDto task)
    {
        await _taskService.UpdateTask(task);
        return NoContent();
    }
    
    [HttpGet("PerUser/{id}")]
    public async Task<ActionResult<List<TaskDto>>> GetAllUserTasks(int id)
    {
        var result = await _service.GetAllUserTasks(id);
        if (result.Any())
            return result;
        else
            return NotFound();
    }

    [HttpGet("FinishedThisYearPerUser/{id}")]
    public async Task<ActionResult<List<FinishedThisYear>>> UsesTasksFinishedThisYear(int id)
    {
        var result = await _service.GetAllUserTasksFinishedThisYear(id);
        if (result.Any())
            return result;
        else
            return NotFound();
    }

    [HttpGet("Proceeding/{id}")]
    public async Task<ActionResult<List<TaskDto>>> GetNotFinishedTasks(int id)
    {
        var result = await _service.GetNotFinishedTasks(id);
        if (result.Any())
            return result;
        else return NotFound();
    }
}