using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

[Route("api/[controller]")]
[ApiController]
public class TeamsController : ControllerBase
{
    private readonly TeamService _teamService;
    private readonly Service _service;

    public TeamsController(TeamService teamService, Service service)
    {
        _service = service;
        _teamService = teamService;
    }

    [HttpGet]
    public async Task<ActionResult<List<TeamDto>>> GetAllTeams()
    {
        return await _teamService.GetAllTeams();       
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<TeamDto>> GetTeam(int id)
    {
        return await _teamService.GetTeam(id);
    }

    [HttpPost]
    public async Task<ActionResult<TeamDto>> PostTeam(TeamDto team)
    {
        try
        {
            await _teamService.PostTeam(team);
            return team;
        }
        catch
        {
            return BadRequest();
        }
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult> DeleteTeam(int id)
    {
        try
        {
            await _teamService.DeleteTeam(id);
            return NoContent();
        }
        catch
        {
            return BadRequest();
        }
    }

    [HttpPut]
    public async Task<ActionResult> UpdateTeam(TeamDto team)
    {
        await _teamService.UpdateTeam(team);
        return NoContent();
    }

    [HttpGet("WithUsersOlderTen")]
    public async Task<ActionResult<List<TeamWithUsersOlderTen>>> GetTeamsWithUsersOlderTen()
    {
        return await _service.GetTeamsWithUsersOlderTen();
    }
}