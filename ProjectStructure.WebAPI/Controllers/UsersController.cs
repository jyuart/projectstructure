using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

[Route("api/[controller]")]
[ApiController]
public class UsersController : ControllerBase 
{
    private readonly UserService _userService;
    private readonly Service _service;

    public UsersController(UserService userService, Service service) 
    {
        _userService = userService;
        _service = service;
    }

    [HttpGet]
    public async Task<ActionResult<List<UserDto>>> GetAllUsers()
    {
        return await _userService.GetAllUsers();
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<UserDto>> GetUser(int id)
    {
        return await _userService.GetUser(id);
    }

    [HttpPost]
    public async Task<ActionResult<UserDto>> PostUser(UserDto user)
    {
        try
        {
            await _userService.PostUser(user);
            return user;
        }
        catch
        {
            return BadRequest();
        }
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult> DeleteUser(int id)
    {
        try
        {
            await _userService.DeleteUser(id);
            return NoContent();
        }
        catch
        {
            return BadRequest();
        }
    }

    [HttpPut]
    public async Task<ActionResult> UpdateUser(UserDto user)
    {
        await _userService.UpdateUser(user);
        return NoContent();
    }
    
    [HttpGet("WithTasks")]
    public async Task<ActionResult<List<UserTasks>>> UsersWithTasks()
    {
        return await _service.GetAllUsersWithTasks();
    }

    [HttpGet("Details/{id}")]
    public async Task<ActionResult<UserDetails>> GetUserDetails(int id)
    {
        var result = await _service.GetUserDetails(id);
        if (result != null)
            return result;
        else
            return NotFound();
    }
}